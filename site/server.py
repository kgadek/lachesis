from bottle import route, run, static_file, response  # http://gotofritz.net/blog/weekly-challenge/restful-python-api-bottle/
import sqlite3
from contextlib import contextmanager
import json
import datetime
import collections as clts


@contextmanager
def closing(thing):
    try:
        yield thing
    finally:
        thing.close()

def unix_time(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()

def unix_time_millis(dt):
    return int(unix_time(dt) * 1000)

# ----------------------------------------------------------------------------------------------------------------------

@route('/static/<filepath:path>')
def get_server_static(filepath):
    return static_file(filepath, root='static')

@route('/json')
def get_json():
    output_json = clts.defaultdict(dict)

    with closing(sqlite3.connect('../daemon/lachesis.sqlite3')) as conn:
        with closing(conn.cursor()) as c:
            for row in c.execute('select * from lachesis where key = "Lua:cpu.main";'):
                epoch_ms = unix_time_millis(datetime.datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S.%f"))
                raw_data = json.loads(row[3])
                for cpuid, raw_data in raw_data.items():
                    output_json["Idle"][epoch_ms] = float(raw_data['idle'])
                    if cpuid != "all":
                        usageunits_total = sum(units for key, units in raw_data.items())
                        output_json["cpu-{}".format(cpuid)][epoch_ms] = float(usageunits_total - raw_data['idle'])

    output_array = [
        {
            "name": cpukey,
            "data": [
                [k, output_json[cpukey][k]]
                for k in sorted(output_json[cpukey].iterkeys())
            ]
        }
        for cpukey in sorted(output_json.iterkeys())
    ]

    response.content_type = 'application/json'
    return json.dumps(output_array)

run(host='0.0.0.0', port=8080, debug=True)

