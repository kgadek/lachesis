#!/bin/sh

export CXX=/usr/bin/clang++
export CC=/usr/bin/clang


mkdir -p build-clang
rm -rf build-clang/*
cd build-clang
cmake -DUSE_CLANG=ON -D_CMAKE_TOOLCHAIN_PREFIX=llvm- -DCMAKE_BUILD_TYPE=Release ..
cd ..


mkdir -p build-clang-dbg
rm -rf build-clang-dbg/*
cd build-clang-dbg
cmake -DUSE_CLANG=ON -D_CMAKE_TOOLCHAIN_PREFIX=llvm- -DCMAKE_BUILD_TYPE=Debug ..
cd ..
