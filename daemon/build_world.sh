#!/bin/sh

for bldtype in build-clang build-clang-dbg build-gcc build-gcc-dbg
do
    make -C "${bldtype}"
done