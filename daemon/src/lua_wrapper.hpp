#ifndef LACHESIS_LUA_HPP
#define LACHESIS_LUA_HPP


class Lua {
    unique_ptr<lua_State, decltype(&lua_close)> _lua;

public:
    explicit Lua() : _lua(nullptr, &lua_close) {
        lua_State *lua = luaL_newstate();
        if( ! lua ) 
            throw "Could not initialize Lua! :(";
        luaL_openlibs(lua);
        _lua = unique_ptr<lua_State, decltype(&lua_close)>(lua, &lua_close);
    }

    inline operator lua_State*() {
        return _lua.get();
    }

    ~Lua() {
        cout << "destroy lua";
    }

    void loadfile(const char *file) {
        lua_State *lua = (*this);
        if(luaL_loadfile(lua, file)) {
            ostringstream ss;
            ss << "Error while loading file: " << lua_tostring(lua, -1);
            lua_pop(lua, 1);
            throw ss.str();
        }
        if(lua_pcall(lua, 0, LUA_MULTRET, 0)) {
            ostringstream ss;
            ss << "Error while executing script: " << lua_tostring(lua, -1);
            lua_pop(lua, 1);
            throw ss.str();
        }
    }

    auto _run_res(tuple<> &x) -> tuple<>& {
        return x;
    }

    template <class ...Res>
    auto _run_res(tuple<int, Res...> &x) -> tuple<int, Res...>& {
        get<0>(x) = lua_tonumber((*this), -1);
        lua_pop((*this), 1);
        using TupTail = tuple<Res...>;
        _run_res(forward<TupTail&>((TupTail&)x));
        return x;
    }

    template <class ...Res>
    auto _run_res(tuple<string, Res...> &x) -> tuple<string, Res...>& {
        string tmp {lua_tostring((*this), -1)};
        get<0>(x) = tmp;
        lua_pop((*this), 1);
        using TupTail = tuple<Res...>;
        _run_res(forward<TupTail&>((TupTail&)x));
        return x;
    }

    void _run_args() {}

    template <class ...Args>
    void _run_args(string head, Args&&... args) {
        lua_pushstring((*this), head.c_str());
        _run_args(forward<Args>(args)...);
    }

    template <class ...Args>
    void _run_args(int head, Args&&... args) {
        lua_pushnumber((*this), head);
        _run_args(forward<Args>(args)...);
    }

    template <class ...Res, class ...Args>
    tuple<Res...> run(const char *funname, Args&&... args) {
        lua_getglobal((*this), funname);
        _run_args(forward<Args>(args)...);
        tuple<Res...> result;
        lua_pcall((*this), sizeof...(Args), sizeof...(Res), 0);
        return _run_res(result);
    }
};


#endif




