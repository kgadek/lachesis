#include "sqlite.hpp"

#include <sstream>

using std::ostringstream;


int LachesisSQLite3db::exec(const char* sql) {
    char *zErrMsg = nullptr;
    int conn = sqlite3_exec(_db.get(), sql, nullptr, 0, &zErrMsg);
    if(conn != SQLITE_OK) {
        ostringstream ss;
        ss << "LachesisSQLite3db exec error! :(\n" << zErrMsg;
        sqlite3_free(zErrMsg);
        throw ss.str();
    }
    return conn;
}

LachesisSQLite3db::LachesisSQLite3db(const char *db_path)
        : _db(nullptr, &sqlite3_close), _insertStmt(nullptr, &sqlite3_finalize) {
    sqlite3 *db;
    if( sqlite3_open(db_path, &db) ) {
        ostringstream ss;
        ss << "LachesisSQLite3db open error! :(\n" << sqlite3_errmsg(_db.get());
        throw ss.str();
    }

    _db = std::unique_ptr<sqlite3, decltype(&sqlite3_close)>(db, &sqlite3_close);

    exec(
        "create table if not exists lachesis("
            "id integer not null unique primary key autoincrement,"
            "timestamp text constraint time_default default (strftime('%Y-%m-%d %H:%M:%f')),"
            "key text,"
            "value text"
        ");"
    );
    exec(
        "create index if not exists lachesis_timestamp_idx on lachesis(timestamp desc);"
    );
    
    sqlite3_stmt *insertStmt;
    if( sqlite3_prepare_v2(_db.get(), "insert into lachesis(key,value) values (?, ?)", -1, &insertStmt, nullptr) )
        throw "LachesisSQLite3db exec error! :(";
    _insertStmt = std::unique_ptr<sqlite3_stmt, decltype(&sqlite3_reset)>(insertStmt, &sqlite3_finalize);
}

void LachesisSQLite3db::store(const char *key, const char *value) {
    sqlite3_bind_text(_insertStmt.get(), 1, key,   -1, SQLITE_STATIC);
    sqlite3_bind_text(_insertStmt.get(), 2, value, -1, SQLITE_STATIC);
    sqlite3_step(_insertStmt.get()); // silently drop data on errors
    sqlite3_reset(_insertStmt.get());
}





