#include <cstdio>
#include <cstdlib>
#include <memory>
#include <utility>
#include <iostream>
#include <queue>
#include <vector>
#include <boost/optional.hpp>
#include <lua.hpp>
#include <sstream>
#include <string>

#include "task.hpp"
#include "lua_wrapper.hpp"
#include "sqlite.hpp"


using namespace std;
using std::unique_ptr;
using std::ostringstream;


class TaskLua : public Task {
    Lua _lua;
public:
    TaskLua() : _lua{} {
        _lua.loadfile("wrapper.lua");
        _lua.run<string, int>("call0", "cpu.main");
    }
    optional<string> run() {
        auto res = _lua.run<string, int>("call0", "cpu.main");
        wakeupTime_update();
        return get<1>(res) ? optional<string>(get<0>(res)) : optional<string>();
    }
    const char* taskname() const {
        return "Lua:cpu.main";
    }
};


int main(int, char const *[]) try {
    using TaskPtr = shared_ptr<Task>;
    priority_queue<TaskPtr, vector<TaskPtr>, Task::Comparator> pqueue;

    pqueue.push(TaskPtr(new TaskLua{}));

    LachesisSQLite3db db("lachesis.sqlite3");

    while(!pqueue.empty()) {
        auto task = pqueue.top();
        pqueue.pop();
        struct timespec waketime = task->wakeupTime();
        while(clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &waketime, nullptr)) {
            ; // loop until time met
        }
        auto res = task->run();
        if(res) {
            db.store(task->taskname(), res->c_str());
        }
        pqueue.push(task);
    }
    return 0;

} catch (std::string & ex) {
    cout << "ERR: " << ex << endl;
    return 1;
} catch (...) {
    cout << "???";
    return 2;
}

