#ifndef LACHESIS_SQLITE_HPP
#define LACHESIS_SQLITE_HPP

#include <sqlite3.h>
#include <string>
#include <memory>


class LachesisSQLite3db {
    std::unique_ptr<sqlite3, decltype(&sqlite3_close)> _db;
    std::unique_ptr<sqlite3_stmt, decltype(&sqlite3_reset)> _insertStmt;

    int exec(const char* sql);

public:
    LachesisSQLite3db(const char *db_path);

    void store(const char *key, const char *value);
};



#endif
