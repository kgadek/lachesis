#ifndef LACHESIS_TASK_HPP
#define LACHESIS_TASK_HPP

#include <ctime>
#include <iostream>
#include <memory>
#include <boost/optional.hpp>
#include <iostream>

using namespace std;
using boost::optional;


ostream& operator<<(ostream &out, const struct timespec &tm);


class Task {
protected:
    inline long int compare(const Task &other) const {
        if(_wakeupTime.tv_sec == other._wakeupTime.tv_sec)
            return _wakeupTime.tv_nsec - other._wakeupTime.tv_nsec;
        return _wakeupTime.tv_sec - other._wakeupTime.tv_sec;
    }

    void wakeupTime_update() throw() {
        clock_gettime(CLOCK_MONOTONIC, &_wakeupTime);
        _wakeupTime.tv_sec += sec_diff;
        _wakeupTime.tv_nsec += nsec_diff;
        if(_wakeupTime.tv_nsec < 0L || _wakeupTime.tv_nsec > 999999999L) {
            _wakeupTime.tv_sec += 1L;
            _wakeupTime.tv_nsec -= 1000000000L;
        }
    }

    struct timespec _wakeupTime = {0L, 0L};

    long int sec_diff = 1L;
    long int nsec_diff = 0L;

public:
    virtual ~Task() throw();
    
    virtual optional<string> run()                   = 0;
    virtual const char* taskname() const             = 0;
    virtual const struct timespec wakeupTime() const {
        return _wakeupTime;
    }

    struct Comparator {
        bool operator()(const shared_ptr<Task> &a, const shared_ptr<Task> &b) {
            return *a > *b;
        }
    };

    bool operator<(const Task &other) const;
    
    bool operator>(const Task &other) const;
};


#endif




