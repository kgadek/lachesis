#include "task.hpp"

using namespace std;

Task::~Task() throw() {}


bool Task::operator<(const Task &other) const {
    return compare(other) < 0;
}

bool Task::operator>(const Task &other) const {
    return compare(other) > 0;
}
