==============================================================================
 Lachesis :: Daemon
==============================================================================
------------------------------------------------------------------------------
 Daemon monitorujący napisany w C++
------------------------------------------------------------------------------



Kompilacja i testy
==================

Kompilacja z użyciem `g++`
--------------------------

Przygotować środowisko poprzez uruchomienie skryptu `prepare-gcc.sh`. Następnie:
    
    make -C build-gcc



Kompilacja z użyciem `clang++`
------------------------------

Przygotować środowisko poprzez uruchomienie skryptu `prepare-clang.sh`. Następnie:
    
    make -C build-clang


Testy
-----

    ctest -D ExperimentalTest
    ctest -D ExperimentalMemCheck
    
    cppcheck --enable=all --std=c++11 --suppress=missingIncludeSystem src




Sublime Text 2/3 + SublimeClang
===============================

Jeśli ktoś korzysta, to by linter dobrze działał należy dodać do `SublimeClang Settings - User` dodać:

    {
        "add_language_option": true,
        "additional_language_options": {
            "c++": ["-std=c++11"]
        }
    }
