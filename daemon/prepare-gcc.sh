#!/bin/sh

export CXX=/usr/bin/g++
export CC=/usr/bin/gcc

mkdir -p build-gcc
rm -rf build-gcc/*
cd build-gcc
cmake -DCMAKE_BUILD_TYPE=Release ..
cd ..

mkdir -p build-gcc-dbg
rm -rf build-gcc-dbg/*
cd build-gcc-dbg
cmake -DCMAKE_BUILD_TYPE=Debug ..
cd ..


# cmake -DCMAKE_CXX_COMPILER=/usr/lib64/clang-analyzer/scan-build/c++-analyzer \
#       -DCMAKE_C_COMPILER=/usr/lib64/clang-analyzer/scan-build/ccc-analyzer ..
