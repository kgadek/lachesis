local cpu = {}

local JSON = require "plugins.JSON"

-- -----------------------------------------------------------------------------

local function readFile(file)
    local f = io.open(file, "rb")
    local content = f:read("*all")
    f:close()
    return content
end

local function unlines( str )
    return str:gmatch("[^\r\n]+")
end

local function foldr_i(func, val, itr)
    for i in itr do
        val = func(val, i)
    end
    return val
end

function string.starts(String,Start)
    return string.sub(String,1,string.len(Start))==Start
end

-- -----------------------------------------------------------------------------

local prev = {}

local function readstat(str)
    local res = {}
    local iter = str:gmatch("%S+")
    _, _, cpuId = string.find(iter(), "cpu(%d+)")
    cpuId = cpuId or "all"
    res["user"] = tonumber(iter())
    res["nice"] = tonumber(iter())
    res["system"] = tonumber(iter())
    res["idle"] = tonumber(iter())
    -- There are even more fields since kernel 2.5.41 and newer. They are
    -- superfluous for this task, so we only sum them.
    res["other"] = foldr_i(function (x,y) return x+tonumber(y) end, 0, iter)
    if not prev[cpuId] then
        prev[cpuId] = res
        return nil, nil
    end
    local res2 = {}
    for k, v in pairs(res) do
        res2[k] = v - prev[cpuId][k]
    end
    prev[cpuId] = res
    return cpuId, res2
end


function cpu.main()
    local res = {}
    for line in unlines(readFile("/proc/stat")) do
        if string.starts(line, "cpu") then
            cpuId, cpuData = readstat(line)
            if cpuId then 
                res[cpuId] = cpuData
            end
        else
            break
        end
    end
    if next(res) == nil then
        return 0, ""
    end
    return 1, JSON:encode(res)
end

return cpu

